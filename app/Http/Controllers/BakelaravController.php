<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bakelarav;

class BakelaravController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $bakelaravs = Bakelarav::all();

        return view('bakelistes.index', compact('bakelaravs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('bakelistes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'bakeliste'=>'required',
            'email'=>'required',
            'description'=>'required',
        ]);

        if ($this == TRUE){
            $req = $request;
        }

        $bakelaravUser = new Bakelarav([
            'bakeliste' => $req->get('bakeliste'),
            'email' => $req->get('email'),
            'description' => $req->get('description'),
        ]);
        $bakelaravUser->save();
        return redirect('/bakelarav')->with('success', 'Bakeliste Enregistre!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $bakelarav = Bakelarav::find($id);
        return view('bakelistes.edit', compact('bakelarav'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'bakeliste'=>'required',
            'description'=>'required',
            'email'=>'required',
        ]);

        if ($this == TRUE){
            $req = $request;
        }

        $bakelarav = Bakelarav::find($id);
        $bakelarav->bakeliste =  $req->get('bakeliste');
        $bakelarav->email = $req->get('email');
        $bakelarav->description = $req->get('description');
        $bakelarav->save();

        return redirect('/bakelarav')->with('success', 'Bakeliste Modifié!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $bakelarav = Bakelarav::find($id);
        $bakelarav->delete();

        return redirect('/bakelarav')->with('success', 'Bakeliste Supprimé!');
    }
}
