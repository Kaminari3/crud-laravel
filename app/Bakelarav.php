<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bakelarav extends Model
{
    //
    protected $fillable = [
        'bakeliste',
        'email',
        'description',      
    ];
}
