@extends('base') 
@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Modification Bakeliste</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form method="post" action="{{ route('bakelarav.update', $bakelarav->id) }}">
            <!-- @method('PATCH') 
            @csrf -->
            {{!! method_field('PATCH') !!}} 
            {{!! csrf_field() !!}}
            <div class="form-group">

                <label for="bakeliste">Nom Bakeliste:</label>
                <input type="text" class="form-control" name="bakeliste" value={{ $bakelarav->bakeliste }} />
            </div>
            
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="text" class="form-control" name="email" value={{ $bakelarav->email }} />
            </div>
            <div class="form-group">
                <label for="description">Description:</label>
                <input type="text" class="form-control" name="description" value={{ $bakelarav->description }} />
            </div>
            <button type="submit" class="btn btn-primary">Modifier</button>
        </form>
    </div>
</div>
@endsection