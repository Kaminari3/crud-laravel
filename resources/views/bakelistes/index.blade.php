@extends('base')

@section('main')
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Bakelistes</h1>    
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Nom</td>
          <td>Email</td>
          <td>Description</td>
          <td colspan = 2>Actions</td>
        </tr>
        <div>
          <a style="margin: 19px;" href="{{ route('bakelarav.create')}}" class="btn btn-primary">New Bakeliste</a>
        </div>
    </thead>

    
    <tbody>
        @foreach($bakelaravs as $bakelarav)
        <tr>
            <td>{{$bakelarav->id}}</td>
            <td>{{$bakelarav->bakeliste}}</td>
            <td>{{$bakelarav->email}}</td>
            <td>{{$bakelarav->description}}</td>
            <td>
                <a href="{{ route('bakelarav.edit',$bakelarav->id)}}" class="btn btn-primary">Modifier</a>
            </td>
            <td>
                <form action="{{ route('bakelarav.destroy', $bakelarav->id)}}" method="post">
                <!-- les cotes sont valables pour la version laravel 5.1 pour les versions
                 superieurs a 5.6 ont mets le @csrf et @method('delete') -->
                {!! csrf_field() !!}
                {!! method_field('DELETE') !!}
                  <button class="btn btn-danger" type="submit">Supprimer</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
</div>
<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
</div>
@endsection
