@extends('base')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Ajouter un Bakeliste</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('bakelarav.store') }}">
          {!! csrf_field() !!}
          <div class="form-group">    
              <label for="bakeliste">Nom du bakeliste:</label>
              <input type="text" class="form-control" name="bakeliste"/>
          </div>
       
          <div class="form-group">
              <label for="email">Email:</label>
              <input type="text" class="form-control" name="email"/>
          </div>
          <div class="form-group">
              <label for="description">Description:</label>
              <input type="text" class="form-control" name="description"/>
          </div>
          
                                 
          <button type="submit" class="btn btn-primary-outline">Ajouter Bakeliste</button>
      </form>
  </div>
</div>
</div>
@endsection