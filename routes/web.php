<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*---routes avec arguments ou noms----*/
Route::get('hello', function () {
    return 'welcome people';
});

// Route::get('hello/{name}', function ($name) {
//     return "welcome $name";
// });

Route::get('hello/{name}-{id}', function ($name,$id) {
    return "welcome $name votre ID: $id";
})-> where('name', '[a-z0-9\-]+')-> where('id', '[0-9]+');


/*--- route accueil---*/
Route::get('/', function () {
    return view('welcome');
});

Route::resource('bakelarav', 'BakelaravController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
